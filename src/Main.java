import java.io.FileNotFoundException;

public class Main
{
    public static void main(String[] args) throws FileNotFoundException
    {
        Reader newFile = new Reader("cords.txt");
        PointList list = new PointList();

        list.addPoint(newFile.get1DCoordsFromFile());
        list.addPoint(newFile.get2DCoordsFromFile());
        list.addPoint(newFile.get3DCoordsFromFile());
        list.addPoint(newFile.get1DCoordsFromFile());
        list.addPoint(newFile.get1DCoordsFromFile());
        list.addPoint(newFile.get1DCoordsFromFile());
        list.addPoint(newFile.get2DCoordsFromFile());
        list.addPoint(newFile.get2DCoordsFromFile());
        list.addPoint(newFile.get1DCoordsFromFile());
        list.addPoint(newFile.get3DCoordsFromFile());

        list.printSortedPoint();
    }
}
