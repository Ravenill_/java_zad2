import java.lang.String;
import java.util.Comparator;

public class Point_1D
{
    protected float x;
    private float distance_;

    public Point_1D(float _x)
    {
        x = _x;
        distance_ = x;
    }

    public Point_1D()
    {
        x = 0;
        distance_ = 0;
    }

    public float distance()
    {
        return distance_;
    }

    public static Comparator<Point_1D> ascOrder = new Comparator<Point_1D>()
    {
        public int compare(Point_1D point1, Point_1D point2)
        {
            float dist1 = point1.distance();
            float dist2 = point2.distance();

            if (dist1 < dist2)
                return -1;
            else if (dist1 == dist2)
                return 0;
            else
                return 1;
        }
    };

    @Override
    public String toString()
    {
        return "(" + x + ")";
    }
}
