import java.util.Comparator;

public class Point_2D extends Point_1D
{
    protected float y;
    private float distance_;

    public Point_2D(float _x, float _y)
    {
        super(_x);
        y = _y;
        distance_ = (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public Point_2D()
    {
        super();
        y = 0;
        distance_ = 0;
    }

    public float distance()
    {
        return distance_;
    }

    public static Comparator<Point_2D> ascOrder = new Comparator<Point_2D>()
    {
        public int compare(Point_2D point1, Point_2D point2)
        {
            float dist1 = point1.distance();
            float dist2 = point2.distance();

            if (dist1 < dist2)
                return -1;
            else if (dist1 == dist2)
                return 0;
            else
                return 1;
        }
    };

    @Override
    public String toString()
    {
        return "(" + x + "," + y + ")";
    }
}
