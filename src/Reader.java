import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Reader
{
    private File file;
    private Scanner scan;

    public Reader(String fileName) throws FileNotFoundException
    {
        file = new File(fileName);
        scan = new Scanner(file);
    }

    public Point_3D get3DCoordsFromFile()
    {
        return new Point_3D(scan.nextFloat(), scan.nextFloat(), scan.nextFloat());
    }

    public Point_2D get2DCoordsFromFile()
    {
        return new Point_2D(scan.nextFloat(), scan.nextFloat());
    }

    public Point_1D get1DCoordsFromFile()
    {
        return new Point_1D(scan.nextFloat());
    }
}
