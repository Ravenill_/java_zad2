import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PointList
{
    private List<Point_1D> listOfPoint_1D = new ArrayList<>();
    private List<Point_2D> listOfPoint_2D = new ArrayList<>();
    private List<Point_3D> listOfPoint_3D = new ArrayList<>();

    void addPoint(Point_1D point)
    {
        listOfPoint_1D.add(point);
    }

    void addPoint(Point_2D point)
    {
        listOfPoint_2D.add(point);
    }

    void addPoint(Point_3D point)
    {
        listOfPoint_3D.add(point);
    }

    void printSortedPoint()
    {
        List<Point_3D> tempListOf3D = new ArrayList<>();

        for (Point_1D point : listOfPoint_1D)
        {
            tempListOf3D.add(new Point_3D(point));
        }

        for (Point_2D point : listOfPoint_2D)
        {
            tempListOf3D.add(new Point_3D(point));
        }

        for (Point_3D point : listOfPoint_3D)
        {
            tempListOf3D.add(point);
        }

        Collections.sort(tempListOf3D, Point_3D.ascOrder);

        for (Point_3D point : tempListOf3D)
            System.out.println(point);
    }
}
