import java.util.Comparator;

public class Point_3D extends Point_2D
{
    protected float z;
    private float distance_;

    private boolean is1D;
    private boolean is2D;
    private boolean is3D;

    public Point_3D(float _x, float _y, float _z)
    {
        super(_x, _y);
        z = _z;
        distance_ = (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        is1D = false;
        is2D = false;
        is3D = true;
    }

    public Point_3D()
    {
        super();
        z = 0;
        distance_ = 0;
        is1D = false;
        is2D = false;
        is3D = true;
    }

    public Point_3D(Point_2D point)
    {
        super(point.x, point.y);
        z = 0;
        distance_ = (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        is1D = false;
        is2D = true;
        is3D = false;
    }

    public Point_3D(Point_1D point)
    {
        super(point.x, 0);
        z = 0;
        distance_ = (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        is1D = true;
        is2D = false;
        is3D = false;
    }

    public float distance()
    {
        return distance_;
    }

    public static Comparator<Point_3D> ascOrder = new Comparator<Point_3D>()
    {
        public int compare(Point_3D point1, Point_3D point2)
        {
            float dist1 = point1.distance();
            float dist2 = point2.distance();

            if (dist1 < dist2)
                return -1;
            else if (dist1 == dist2)
                return 0;
            else
                return 1;
        }
    };

    @Override
    public String toString()
    {
        if (is1D)
            return "(" + x + ")";
        else if (is2D)
            return "(" + x + "," + y + ")";
        else
            return "(" + x + "," + y + "," + z +")";
    }
}
